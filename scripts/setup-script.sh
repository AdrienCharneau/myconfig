#!/bin/bash


sudo -E apt update


# install software (GUI)

sudo -E apt install -y feh
sudo -E apt install -y i3
sudo -E apt install -y picom
sudo -E apt install -y rofi


# install software (Terminal Tools)

sudo -E apt install -y btop
sudo -E apt install -y ffmpeg
# sudo -E apt install -y htop
sudo -E apt install -y mpg123
sudo -E apt install -y nano
sudo -E apt install -y net-tools
sudo -E apt install -y openssh-server
sudo -E apt install -y psmisc
sudo -E apt install -y rsync
sudo -E apt install -y samba
sudo -E apt install -y screenfetch
sudo -E apt install -y xinput


# install software (Graphic Tools)

sudo -E apt install -y blueman
# sudo -E apt install -y cool-retro-term
sudo -E apt install -y eom
sudo -E apt install -y file-roller
sudo -E apt install -y gitg
sudo -E apt install -y gparted
sudo -E apt install -y hardinfo
sudo -E apt install -y lxappearance
sudo -E apt install -y mpv
sudo -E apt install -y pavucontrol
sudo -E apt install -y retext
sudo -E apt install -y simple-scan
sudo -E apt install -y terminator
sudo -E apt install -y thunar
sudo -E apt install -y vlc


# remove gnome keyring

sudo -E rm -r -v ~/.local/share/keyrings/*
sudo -E apt purge gnome-keyring


# refresh script

cd ~/myconfig/scripts
sudo -E chmod +x -v display-manager-script.sh
sudo -E chmod +x -v refresh-script.sh
sudo -E chmod +x -v myupdate-script.sh
sudo -E chmod +x -v mycleanup-script.sh

sudo -E chmod +x -v brave-backup-scripts/session-backup-script.sh
sudo -E chmod +x -v brave-backup-scripts/session-restore-script.sh
sudo -E chmod +x -v brave-backup-scripts/bookmarks-backup-script.sh
sudo -E chmod +x -v brave-backup-scripts/bookmarks-restore-script.sh

sudo -E chmod +x -v cache-cleanup-scripts/recent-files.sh
sudo -E chmod +x -v cache-cleanup-scripts/spotify.sh

sudo -E chmod +x -v data-backup-scripts/mybackup-script.sh
sudo -E chmod +x -v data-backup-scripts/myrestore-script.sh
sudo -E chmod +x -v data-backup-scripts/familyrestore-script.sh

sudo -E chmod +x -v data-backup-scripts/mybackup-dry-script.sh
sudo -E chmod +x -v data-backup-scripts/myrestore-dry-script.sh
sudo -E chmod +x -v data-backup-scripts/familyrestore-dry-script.sh


# prevents terminal from closing

$SHELL
